﻿DROP DATABASE IF EXISTS b20190528;
CREATE DATABASE b20190528;
USE b20190528;

/* CREANDO TABLA DE LOS ANIMALES */

CREATE TABLE animales(
  id int AUTO_INCREMENT,  /*AQUI PUEDO COMENTAR TAMBIEN*/
  nombre varchar(100), 
  raza varchar(100),
  fechaNac date,
  PRIMARY KEY (id)
);

/* CREANDO TABLA DE LOS VETERINARIOS */

CREATE TABLE veterinarios(
  cod int AUTO_INCREMENT,
  nombre varchar(100),
  especialidad varchar(100),
  PRIMARY KEY (cod)
);

/* CREANDO TABLA DE LA RELACION ACUDEN */

CREATE TABLE acuden(
  idanimal int,
  codVet int, 
  fecha date,
  PRIMARY KEY (idanimal, codVet,fecha),
  UNIQUE KEY(idanimal),
  CONSTRAINT fkacudenanimales FOREIGN KEY (idanimal) REFERENCES animales (ID),
  CONSTRAINT fkacudenveterinarios FOREIGN KEY (codVet) REFERENCES veterinarios (cod)
);

/*insertar un registro*/

INSERT INTO animales (nombre, raza, fechaNac)
  VALUES ('Jorge', 'bulldog', '2000/2/1'),
('alma', 'podenco', '2014/2/14');

-- listar

SELECT * FROM animales;

